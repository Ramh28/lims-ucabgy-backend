from rest_framework import serializers


class LaboratoryStaffSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    username = serializers.CharField(max_length=100)
    email = serializers.EmailField()
    first_name = serializers.CharField(max_length=100)
    last_name = serializers.CharField(max_length=100)
    cdi = serializers.CharField(max_length=20)
    status = serializers.CharField(max_length=3)
    role = serializers.CharField(max_length=4)


class PhoneNumberSerializer(serializers.Serializer):
    number = serializers.CharField(max_length=15)
    laboratory_staff = LaboratoryStaffSerializer()


class EquipmentSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=100)
    description = serializers.CharField(max_length=100)
    brand = serializers.CharField(max_length=50)
    model = serializers.CharField(max_length=50)
    serial_number = serializers.CharField(max_length=50)
    introduction_date = serializers.DateField()
    location = serializers.CharField(max_length=50)
    features = serializers.JSONField()

    admin = LaboratoryStaffSerializer()


class EquipmentMaintenanceSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    Equipment = EquipmentSerializer()
    scheduled_date = serializers.DateField()
    done_date = serializers.DateField()
    next_maintenance_date = serializers.DateField()
    maintenance_details = serializers.JSONField()
    agent_name = serializers.CharField(max_length=50)
    status = serializers.CharField(max_length=10)


class EquipmentCalibrationSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    Equipment = EquipmentSerializer()
    last_calibration_date = serializers.DateField()
    scheduled_date = serializers.DateField()
    done_date = serializers.DateField()
    next_calibration_date = serializers.DateField()
    calibration_details = serializers.JSONField()
    agent_name = serializers.CharField(max_length=50)
    status = serializers.CharField(max_length=10)


class ClientSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    contact_name = serializers.CharField(max_length=50)
    contact_lastname = serializers.CharField(max_length=50)
    email = serializers.EmailField()
    contact_phone = serializers.CharField(max_length=15)
    cdi = serializers.CharField(max_length=20)


class QuoteSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    client_identifier = ClientSerializer()
    total_cost = serializers.DecimalField(max_digits=10, decimal_places=2)
    status = serializers.CharField(max_length=10)
    creation_date = serializers.DateField()
    conversation_images = serializers.ImageField()


class EssaySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    metadata = serializers.JSONField()
    input_fields = serializers.JSONField()
    calculations_forms = serializers.JSONField()


class SampleTypeSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    description = serializers.CharField(max_length=100)


class SampleSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    sample_name = serializers.CharField(max_length=50)
    sample_description = serializers.CharField(max_length=100)
    sample_type = SampleTypeSerializer()


class SamplePerQuoteSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    Sample = SampleSerializer()
    Quote = QuoteSerializer()
    received_by = LaboratoryStaffSerializer()
    received_date = serializers.DateField()
    HasItBeenReceived = serializers.BooleanField()
    amount = serializers.IntegerField()


class EssayPerSampleSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    Sample = SamplePerQuoteSerializer()
    Essay = EssaySerializer()
    assigned_to = LaboratoryStaffSerializer()
    scheduled_date = serializers.DateField()
    done_date = serializers.DateField()
    status = serializers.CharField(max_length=10)
    results = serializers.JSONField()


class MaterialSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    description = serializers.CharField(max_length=100)
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, min_value=0)


class MaterialOrderSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    Materials = MaterialSerializer()
    LaboratoryStaff = LaboratoryStaffSerializer()
    amount = serializers.DecimalField(max_digits=10, decimal_places=2)
    order_date = serializers.DateField()


class MaterialUseSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    Materials = MaterialSerializer()
    LaboratoryStaff = LaboratoryStaffSerializer()
    amount = serializers.DecimalField(max_digits=10, decimal_places=2)
    use_date = serializers.DateField()
    use_details = serializers.CharField()
