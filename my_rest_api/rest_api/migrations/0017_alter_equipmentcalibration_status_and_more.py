# Generated by Django 5.0.1 on 2024-02-25 23:23

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rest_api', '0016_merge_20240225_1919'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipmentcalibration',
            name='status',
            field=models.CharField(choices=[('COMPLETED', 'COMPLETED'), ('TODO', 'TODO'), ('OVERDUE', 'OVERDUE')], default='TODO', max_length=10),
        ),
        migrations.AlterField(
            model_name='equipmentmaintenance',
            name='status',
            field=models.CharField(choices=[('COMPLETED', 'COMPLETED'), ('TODO', 'TODO'), ('OVERDUE', 'OVERDUE')], default='TODO', max_length=10),
        ),
        migrations.AlterField(
            model_name='essaypersample',
            name='assigned_to',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='essaypersample',
            name='results',
            field=models.JSONField(default=None, null=True),
        ),
        migrations.AlterField(
            model_name='essaypersample',
            name='status',
            field=models.CharField(choices=[('COMPLETED', 'COMPLETED'), ('TODO', 'TODO'), ('OVERDUE', 'OVERDUE'), ('UNASSIGNED', 'UNASSIGNED')], default='UNASSIGNED', max_length=10),
        ),
        migrations.AlterField(
            model_name='laboratorystaff',
            name='role',
            field=models.CharField(choices=[('ADMN', 'Admin'), ('LABS', 'Laboratory Staff'), ('LABH', 'Laboratory Head')], default='LABS', max_length=4),
        ),
        migrations.AlterField(
            model_name='laboratorystaff',
            name='status',
            field=models.CharField(choices=[('INA', 'Inactive'), ('ACT', 'Active'), ('RET', 'Retired')], default='ACT', max_length=3),
        ),
        migrations.AlterField(
            model_name='sampleperquote',
            name='received_by',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
