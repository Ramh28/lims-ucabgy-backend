# Generated by Django 5.0.1 on 2024-02-15 20:53

from django.db import migrations


def register_essays(apps, schema_editor):
    # Register Essay " Determinacion de la densidas y la absorcion de agregados finos ".

    essay = apps.get_model('rest_api', 'Essay')

    # register Essay " Determinacion de la densidas y la absorcion de agregados finos ".
    essay_data = essay.objects.create(
        metadata={
            "name": "Determinacion de la densidad y la absorcion de agregados finos",
            "description": "Este manual contempla como objetivo desarrollar el procedimiento de ensayo para "
                           "determinar la densidad aparente, la densidad aparente con muestras saturadas y de "
                           "superficie seca (densidad aparente SSS), la densidad nominal (todas a 23 °C ± 2 °C) y la "
                           "absorción(después de 24 horas en agua) del agregado fino.",
            "cost": 300.00,
            "regulation": "NORMA VENEZOLANA COVENIN 268:1998",
            "sample_type": "Agregados finos",
        },
        input_fields=[
            {
                "field_name": "Densidad del agua aproximada a (1g/ml)",
                "field_type": "number",
                "field_symbol": "d",
                "field_unit": "g/ml",
            },
            {
                "field_name": "Volumen del agua añadida al picnómetro en ml",
                "field_type": "number",
                "field_symbol": "V_a",
                "field_unit": "ml",
            },
            {
                "field_name": "Masa de la muestra saturada y de superficie seca en g",
                "field_type": "number",
                "field_symbol": "M",
                "field_unit": "g",
            },
            {
                "field_name": "Masa del picnómetro vacío en g",
                "field_type": "number",
                "field_symbol": "M_o",
                "field_unit": "g",
            },
            {
                "field_name": "Volúmen del picnómetro en ml",
                "field_type": "number",
                "field_symbol": "V",
                "field_unit": "ml",
            },
            {
                "field_name": "Masa de la muestra en el aire secado en horno en g",
                "field_type": "number",
                "field_symbol": "M_1",
                "field_unit": "g",
            },
        ],
        calculations_forms=[
            {
                "formula_name": "Masa total del picnómetro con la muestra y el agua en g",
                "formula": "(d)(V_a)+(M)-(M_o)",
                "formula_symbol": "M_p"
            },
            {
                "formula_name": "Masa del picnómetro lleno con agua a 23 °C ± 2 °C en g",
                "formula": "(d)(V)+(M_o)",
                "formula_symbol": "M_a"
            },
            {
                "formula_name": "Densidad aparente a 23 °C ± 2 °C en g/ml",
                "formula": "((d) * (M_1))/((M_a) + (M) - (M_p))",
                "formula_symbol": "P_a"
            },
            {
                "formula_name": "Densidad aparente con muestras saturadas y de superficie seca (SSS) en g/ml",
                "formula": "((d) * (M))/((M_a) + (M) - (M_p))",
                "formula_symbol": "P_s"
            },
            {
                "formula_name": "Densidad nominal a 23 °C ± 2 °C  en g/ml",
                "formula": "((d) * (M_1))/((M_a) + (M_1) - (M_p))",
                "formula_symbol": "P"
            },
            {
                "formula_name": "Absorción en %",
                "formula": "( ((M)-(M_1)) / (M_1) ) * 100",
                "formula_symbol": "A"
            },
        ]
    )

    # Register Essay " Determinacion de la densidad y la absorcion de agregados gruesos ".


class Migration(migrations.Migration):
    dependencies = [
        ('rest_api', '0009_alter_equipment_description_and_more'),
    ]

    operations = [
        migrations.RunPython(register_essays),
    ]
