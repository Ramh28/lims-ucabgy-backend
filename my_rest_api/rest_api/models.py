from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser
from datetime import date

phone_regex = RegexValidator(
    regex=r'^\+58\d{10}$',
    message='Phone number must be entered in the format: +589999999999'
)


# User and Laboratory Staff info.

class LaboratoryStaff(AbstractUser):
    cdi = models.CharField(max_length=20, unique=True)

    ADMIN = 'ADMN'
    LABORATORY_HEAD = 'LABH'
    LABORATORY_STAFF = 'LABS'

    role_choices = {
        (ADMIN, 'Admin'),
        (LABORATORY_HEAD, 'Laboratory Head'),
        (LABORATORY_STAFF, 'Laboratory Staff')
    }

    role = models.CharField(
        max_length=4,
        choices=role_choices,
        default=LABORATORY_STAFF
    )

    ACTIVE = 'ACT'
    INACTIVE = 'INA'
    RETIRED = 'RET'

    status_choices = {
        (ACTIVE, 'Active'),
        (INACTIVE, 'Inactive'),
        (RETIRED, 'Retired')
    }

    status = models.CharField(
        max_length=3,
        choices=status_choices,
        default=ACTIVE
    )


class PhoneNumber(models.Model):
    number = models.CharField(
        max_length=15,
        unique=True,
        validators=[phone_regex],
        default='000000000'
    )
    LaboratoryStaff = models.ForeignKey(
        LaboratoryStaff,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )


class Client(models.Model):
    name = models.CharField(max_length=50)
    contact_name = models.CharField(max_length=50)
    contact_lastname = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    contact_phone = models.CharField(
        max_length=15,
        validators=[phone_regex],
        unique=True,
    )
    cdi = models.CharField(max_length=20, unique=True)


class Quote(models.Model):
    client_identifier = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    creation_date = models.DateField(
        default=date.today
    )
    total_cost = models.DecimalField(
        max_digits=10,
        decimal_places=2
    )

    UNPROCESSED = 'UNPROCESSED'
    PROCESSED = 'PROCESSED'
    IN_PROGRESS = 'IN_PROGRESS'

    status_choices = {
        (IN_PROGRESS, 'IN_PROGRESS'),
        (PROCESSED, 'PROCESSED'),
        (UNPROCESSED, 'UNPROCESSED')
    }

    status = models.CharField()
    conversation_images = models.ImageField(
        upload_to='conversation_images',
        default=None,
        null=True
    )


def essay_basic_info():
    return {
        'name': 'Essay name',
        'description': 'Essay description',
        'cost': 0.0,
    }


class Essay(models.Model):
    metadata = models.JSONField(default=essay_basic_info)
    input_fields = models.JSONField()
    calculations_forms = models.JSONField()


class SampleTypes(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=200)


class Samples(models.Model):
    sample_name = models.CharField(max_length=50, default=None, unique=True)
    sample_description = models.CharField(max_length=200, default=None)
    sample_type = models.ForeignKey(
        SampleTypes,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )


class SamplePerQuote(models.Model):
    Sample = models.ForeignKey(
        Samples,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    Quote = models.ForeignKey(
        Quote,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    received_by = models.ForeignKey(
        LaboratoryStaff,
        on_delete=models.CASCADE,
        null=True,
        default=None
    )
    amount = models.IntegerField()
    received_date = models.DateField(
        null=True,
    )
    HasItBeenReceived = models.BooleanField(default=False)


class EssayPerSample(models.Model):
    Essay = models.ForeignKey(
        Essay,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    Sample = models.ForeignKey(
        SamplePerQuote,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    assigned_to = models.ForeignKey(
        LaboratoryStaff,
        on_delete=models.CASCADE,
        default=None,
        null=True
    )

    scheduled_date = models.DateField(
        default=None,
        null=True
    )
    done_date = models.DateField(
        default=None,
        null=True
    )

    COMPLETED = 'COMPLETED'
    OVERDUE = 'OVERDUE'
    TODO = 'TODO'
    UNASSIGNED = 'UNASSIGNED'

    status_choices = {
        (COMPLETED, 'COMPLETED'),
        (OVERDUE, 'OVERDUE'),
        (TODO, 'TODO'),
        (UNASSIGNED, 'UNASSIGNED')
    }
    status = models.CharField(
        max_length=10,
        choices=status_choices,
        default=UNASSIGNED
    )
    results = models.JSONField(
        default=None,
        null=True
    )


def equipment_basic_info():
    return {
        'features': 'Equipment features',
    }


class Equipment(models.Model):
    name = models.CharField(max_length=50, default='Equipment')
    description = models.CharField(max_length=500, default='Equipment description')
    brand = models.CharField(max_length=50, default='Equipment brand')
    model = models.CharField(max_length=50, default='Equipment model')
    serial_number = models.CharField(max_length=50, default='Equipment serial number')
    introduction_date = models.DateField(default=date.today)
    location = models.CharField(max_length=50, default='Equipment location')
    features = models.JSONField(default=equipment_basic_info)
    admin = models.ForeignKey(
        LaboratoryStaff,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )


class EquipmentMaintenance(models.Model):
    Equipment = models.ForeignKey(
        Equipment,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    scheduled_date = models.DateField()
    done_date = models.DateField(default=None)
    next_maintenance_date = models.DateField(default=None)
    maintenance_details = models.CharField(max_length=200, default='Maintenance details')
    agent_name = models.CharField(max_length=50, default='Agent Name')

    COMPLETED = 'COMPLETED'
    OVERDUE = 'OVERDUE'
    TODO = 'TODO'
    status_choices = {
        (COMPLETED, 'COMPLETED'),
        (OVERDUE, 'OVERDUE'),
        (TODO, 'TODO')
    }
    status = models.CharField(
        max_length=10,
        choices=status_choices,
        default=TODO
    )


class EquipmentCalibration(models.Model):
    Equipment = models.ForeignKey(
        Equipment,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    last_calibration_date = models.DateField(default=None)
    scheduled_date = models.DateField(default=None)
    done_date = models.DateField(default=None)
    next_calibration_date = models.DateField(default=None)
    calibration_details = models.CharField(max_length=200, default='Calibration details')
    agent_name = models.CharField(max_length=50, default='Agent Name')

    COMPLETED = 'COMPLETED'
    OVERDUE = 'OVERDUE'
    TODO = 'TODO'
    status_choices = {
        (COMPLETED, 'COMPLETED'),
        (OVERDUE, 'OVERDUE'),
        (TODO, 'TODO')
    }
    status = models.CharField(
        max_length=10,
        choices=status_choices,
        default=TODO
    )


class Materials(models.Model):
    name = models.CharField(max_length=30, default='Material')
    description = models.CharField(max_length=200, default='Material description')
    amount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0
    )


class MaterialOrders(models.Model):
    Materials = models.ForeignKey(
        Materials,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    LaboratoryStaff = models.ForeignKey(
        LaboratoryStaff,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    amount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0
    )
    order_date = models.DateField()


class MaterialUses(models.Model):
    Materials = models.ForeignKey(
        Materials,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    LaboratoryStaff = models.ForeignKey(
        LaboratoryStaff,
        on_delete=models.CASCADE,
        null=False,
        default=None
    )
    amount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        default=0
    )
    use_date = models.DateField()
    use_details = models.CharField(max_length=200)
