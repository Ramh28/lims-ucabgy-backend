from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models

# Essay related classes.


class ObtainAllEssays(APIView):
    def get(self, request):
        essays = models.Essay.objects.all()
        serializer = serializers.EssaySerializer(essays, many=True)

        return Response(serializer.data)


class ObtainEssayById(APIView):
    def get(self, request, essay_id):
        essay = models.Essay.objects.get(id=essay_id)
        serializer = serializers.EssaySerializer(essay)

        return Response(serializer.data)



