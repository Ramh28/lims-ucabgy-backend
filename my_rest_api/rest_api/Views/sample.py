from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models


class ObtainSampleTypes(APIView):
    def get(self, request):
        sample_types = models.SampleTypes.objects.all()
        serializer = serializers.SampleTypeSerializer(sample_types, many=True)

        return Response(serializer.data, status=200)


class AddSampleType(APIView):
    def post(self, request):
        sample_type = models.SampleTypes.objects.create(
            name=request.data['name'],
            description=request.data['description']
        )

        sample_type.save()

        serializer = serializers.SampleTypeSerializer(sample_type)

        return Response(serializer.data, status=201)


class UpdateSampleType(APIView):
    def put(self, request):

        serializer = serializers.SampleTypeSerializer(data=request.data)

        if serializer.is_valid():
            sample_type = models.SampleTypes.objects.get(id=request.data['id'])
            sample_type.name = request.data['name']
            sample_type.description = request.data['description']
            sample_type.save()

            return Response(serializer.data, status=200)

        return Response(serializer.errors, status=400)


class ObtainSamples(APIView):
    def get(self, request):
        samples = models.Samples.objects.all()
        serializer = serializers.SampleSerializer(samples, many=True)

        return Response(serializer.data, status=200)


class AddSample(APIView):
    def post(self, request):
        sample = models.Samples.objects.create(
            sample_name=request.data['sample_name'],
            sample_description=request.data['sample_description'],
            sample_type=models.SampleTypes.objects.get(id=request.data['sample_type'])
        )

        sample.save()

        serializer = serializers.SampleSerializer(sample)

        return Response(serializer.data, status=201)


class UpdateSample(APIView):
    def put(self, request):

        sample = models.Samples.objects.get(id=request.data['id'])
        sample.sample_name = request.data['sample_name']
        sample.sample_description = request.data['sample_description']
        sample.sample_type = models.SampleTypes.objects.get(id=request.data['sample_type'])
        sample.save()

        serializer = serializers.SampleSerializer(sample)

        return Response(serializer.data, status=200)


class ObtainSamplePerQuoteByQuoteID(APIView):
    def get(self, request, quote_id):
        sample_per_quote = models.SamplePerQuote.objects.filter(Quote=quote_id)
        serializer = serializers.SamplePerQuoteSerializer(sample_per_quote, many=True)

        return Response(serializer.data, status=200)


class ObtainSamplePerQuoteByStatus(APIView):
    def get(self, request, status):
        if status == 'true':
            status_prop = True
        else:
            status_prop = False

        sample_per_quote = models.SamplePerQuote.objects.filter(HasItBeenReceived=status_prop)
        serializer = serializers.SamplePerQuoteSerializer(sample_per_quote, many=True)

        return Response(serializer.data, status=200)


class AddSamplePerQuote(APIView):
    def post(self, request):
        quote_id = None
        if request.data['Samples']:
            for sample in request.data['Samples']:
                sample_per_quote = models.SamplePerQuote.objects.create(
                    Sample=models.Samples.objects.get(id=sample['sample_id']),
                    Quote=models.Quote.objects.get(id=sample['quote_id']),
                    received_by=None,
                    amount=sample['amount'],
                )

                essay_per_sample = models.EssayPerSample.objects.create(
                    Essay=models.Essay.objects.get(id=sample['essay_id']),
                    Sample=sample_per_quote,
                )
                quote_id = sample_per_quote.Quote.id
                sample_per_quote.save()
                essay_per_sample.save()
        else:
            return Response({"Message": "No samples to register"}, status=400)

        quote = models.Quote.objects.get(id=quote_id)
        quote.status = 'IN_PROGRESS'
        quote.total_cost = request.data['total_cost']
        quote.save()

        return Response({"Message": "Samples registered correctly"}, status=201)


class ObtainAllSamplesPerQuote(APIView):
    def get(self, request):
        samples_per_quote = models.SamplePerQuote.objects.all()
        serializer = serializers.SamplePerQuoteSerializer(samples_per_quote, many=True)

        return Response(serializer.data, status=200)


class ObtainSamplePerQuoteByID(APIView):

    def get(self, request, sample_per_quote_id):
        samples_per_quote = models.SamplePerQuote.objects.get(id=sample_per_quote_id)
        serializer = serializers.SamplePerQuoteSerializer(samples_per_quote)

        return Response(serializer.data, status=200)


class UpdateSamplePerQuote(APIView):

    def put(self, request):
        sample_per_quote = models.SamplePerQuote.objects.get(id=request.data['id'])
        sample_per_quote.Sample = models.Samples.objects.get(id=request.data['sample_id'])
        sample_per_quote.Quote = models.Quote.objects.get(id=request.data['quote_id'])
        sample_per_quote.received_by = models.LaboratoryStaff.objects.get(email=request.data['received_by'])
        sample_per_quote.received_date = request.data['received_date']
        sample_per_quote.HasItBeenReceived = True

        sample_per_quote.save()
        serializer = serializers.SamplePerQuoteSerializer(sample_per_quote)

        return Response(serializer.data, status=200)
