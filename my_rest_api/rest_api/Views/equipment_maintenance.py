from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models
from datetime import date, timedelta


# Maintenance related classes.
class ObtainAllEquipmentMaintenance(APIView):

    def get(self, request):
        equipment_maintenance = models.EquipmentMaintenance.objects.all()
        serializer = serializers.EquipmentMaintenanceSerializer(equipment_maintenance, many=True)

        return Response(serializer.data)


class ObtainEquipmentMaintenanceByEquipmentId(APIView):

    def get(self, request, equipment_id):
        equipment_maintenance = models.EquipmentMaintenance.objects.filter(Equipment=equipment_id)
        serializer = serializers.EquipmentMaintenanceSerializer(equipment_maintenance, many=True)

        return Response(serializer.data)


class ObtainEquipmentMaintenanceById(APIView):

        def get(self, request, equipment_maintenance_id):
            equipment_maintenance = models.EquipmentMaintenance.objects.get(id=equipment_maintenance_id)
            serializer = serializers.EquipmentMaintenanceSerializer(equipment_maintenance)

            return Response(serializer.data)


class ObtainEquipmentMaintenanceByNextMonth(APIView):
    def get(self, request):
        equipment_maintenance = models.EquipmentMaintenance.objects.filter(
            scheduled_date__range=[date.today(), date.today() + timedelta(days=30)]
        )
        serializer = serializers.EquipmentMaintenanceSerializer(equipment_maintenance, many=True)
        return Response(serializer.data)


class AddNewEquipmentMaintenance(APIView):

    def post(self, request):
        # Creates a new equipment maintenance object.
        equipment_maintenance = models.EquipmentMaintenance.objects.create(
            Equipment=models.Equipment.objects.get(id=request.data['equipment_id']),
            scheduled_date=request.data['scheduled_date'],
            done_date=request.data['done_date'],
            next_maintenance_date=request.data['next_maintenance_date'],
            maintenance_details=request.data['maintenance_details'],
            agent_name=request.data['agent_name'],
            status=request.data['status']
        )

        # Adds the equipment maintenance to the database.
        equipment_maintenance.save()

        return Response({'message': 'Equipment maintenance created successfully'})


class UpdateEquipmentMaintenance(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.
    def post(self, request):
        # Get the equipment maintenance to update.
        equipment_maintenance = models.EquipmentMaintenance.objects.get(id=request.data['id'])

        # Update the equipment maintenance's fields.
        equipment_maintenance.scheduled_date = request.data['scheduled_date']
        equipment_maintenance.done_date = request.data['done_date']
        equipment_maintenance.next_maintenance_date = request.data['next_maintenance_date']
        equipment_maintenance.maintenance_details = request.data['maintenance_details']
        equipment_maintenance.agent_name = request.data['agent_name']
        equipment_maintenance.status = request.data['status']

        # Save the equipment maintenance to the database.
        equipment_maintenance.save()

        return Response({'message': 'Equipment maintenance updated successfully'})

