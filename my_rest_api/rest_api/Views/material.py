from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .. import serializers, models


# material related classes.
class ObtainAllMaterials(APIView):

    def get(self, request):
        materials = models.Materials.objects.all()
        serializer = serializers.MaterialSerializer(materials, many=True)

        return Response(serializer.data)


class UpdateMaterial(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.
    def post(self, request):
        # Get the material to update.
        material = models.Materials.objects.get(id=request.data['id'])

        # Update the material's fields.
        material.name = request.data['name']
        material.description = request.data['description']

        # Save the material to the database.
        material.save()

        return Response({'message': 'material updated successfully'})


class AddNewMaterial(APIView):
    def post(self, request):
        # Creates a new material object.
        material = models.Materials.objects.create(
            description=request.data['description'],
            name=request.data['name']
        )

        # Adds the material to the database.
        material.save()

        return Response({'message': 'material created successfully'})


class ObtainMaterialById(APIView):
    def get(self, request, material_id):
        material = models.Materials.objects.get(id=material_id)
        serializer = serializers.MaterialSerializer(material)

        return Response(serializer.data)

