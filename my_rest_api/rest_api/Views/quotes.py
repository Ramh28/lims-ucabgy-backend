from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models
from datetime import date


# Quote related classes.
class ObtainAllQuotes(APIView):

    def get(self, request):
        quotes = models.Quote.objects.all()
        serializer = serializers.QuoteSerializer(quotes, many=True)

        return Response(serializer.data)


class ObtainQuoteByClientId(APIView):

    def get(self, request, client_id):
        quotes = models.Quote.objects.filter(client_identifier=client_id)
        serializer = serializers.QuoteSerializer(quotes, many=True)

        return Response(serializer.data)


class ObtainQuoteByQuoteId(APIView):

        def get(self, request, quote_id):
            quote = models.Quote.objects.get(id=quote_id)
            serializer = serializers.QuoteSerializer(quote)

            return Response(serializer.data)


class ObtainQuotesByStatus(APIView):

    def get(self, request, status):
        quotes = models.Quote.objects.filter(status=status)
        serializer = serializers.QuoteSerializer(quotes, many=True)

        return Response(serializer.data)


class AddNewQuote(APIView):
    def post(self, request):
        # Creates a new quote object.
        quote = models.Quote.objects.create(
            client_identifier=models.Client.objects.get(id=request.data['client_id']),
            creation_date=request.data['creation_date'] if 'creation_date' in request.data else date.today(),
            total_cost=0.0,
            status='UNPROCESSED',
        )

        serializer = serializers.QuoteSerializer(quote, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)

        return Response(serializer.errors, status=400)


class UpdateQuote(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.
    def post(self, request):
        # Get the quote to update.
        quote = models.Quote.objects.get(id=request.data['id'])

        # Update the quote's fields.
        quote.client_identifier = models.Client.objects.get(id=request.data['client_id'])
        quote.total_cost = request.data['total_cost']
        quote.status = request.data['status']
        quote.conversation_images = request.data['conversation_images']

        # Save the quote to the database.
        quote.save()

        return Response({'message': 'Quote updated successfully'})


class DeleteQuote(APIView):

    def delete(self, request, quote_id):
        quote = models.Quote.objects.get(id=quote_id)
        quote.delete()

        return Response({'message': 'Quote deleted successfully'}, status=204)