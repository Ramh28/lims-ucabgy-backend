from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .. import serializers, models
from datetime import date


# Client related classes.
class ObtainAllClients(APIView):

    def get(self, request):
        clients = models.Client.objects.all()
        serializer = serializers.ClientSerializer(clients, many=True)

        return Response(serializer.data)


class UpdateClient(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.
    def post(self, request):
        # Get the client to update.
        client = models.Client.objects.get(id=request.data['id'])

        # Update the client's fields.
        client.name = request.data['name']
        client.contact_name = request.data['contact_name']
        client.contact_lastname = request.data['contact_lastname']
        client.email = request.data['email']
        client.contact_phone = request.data['contact_phone']
        client.cdi = request.data['cdi']

        # Save the client to the database.
        client.save()

        return Response({'message': 'Client updated successfully'})


class AddNewClient(APIView):
    def post(self, request):
        # Creates a new client object.
        client = models.Client.objects.create(
            name=request.data['name'],
            contact_name=request.data['contact_name'],
            contact_lastname=request.data['contact_lastname'],
            email=request.data['email'],
            cdi=request.data['cdi'],
            contact_phone=request.data['contact_phone']
        )

        # Adds the client to the database.
        client.save()

        return Response({'message': 'Client created successfully'})


class ReceivedClientRequest(APIView):

    permission_classes = [AllowAny]

    def post(self, request):
        # Creates a new client object.
        serializer = serializers.ClientSerializer(data=request.data)

        if serializer.is_valid():
            client = models.Client.objects.create(
                name=serializer.data['name'],
                contact_name=serializer.data['contact_name'],
                contact_lastname=serializer.data['contact_lastname'],
                email=serializer.data['email'],
                cdi=serializer.data['cdi'],
                contact_phone=serializer.data['contact_phone']
            )

            client.save()

            quote = models.Quote.objects.create(
                client_identifier=client,
                creation_date=date.today(),
                total_cost=0,
                status='UNPROCESSED',
            )

            quote.save()

            return Response(serializer.data, status=201)
        else:
            return Response(serializer.errors, status=400)


class ObtainClientById(APIView):
    def get(self, request, client_id):
        client = models.Client.objects.get(id=client_id)
        serializer = serializers.ClientSerializer(client)

        return Response(serializer.data)

