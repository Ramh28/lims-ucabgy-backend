from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models
from datetime import date, timedelta


# Calibration related classes.
class ObtainAllEquipmentCalibration(APIView):

    def get(self, request):
        equipment_calibration = models.EquipmentCalibration.objects.all()
        serializer = serializers.EquipmentCalibrationSerializer(equipment_calibration, many=True)

        return Response(serializer.data)


class ObtainEquipmentCalibrationById(APIView):
    def get(self, request, equipment_calibration_id):
        equipment_calibration = models.EquipmentCalibration.objects.get(id=equipment_calibration_id)
        serializer = serializers.EquipmentCalibrationSerializer(equipment_calibration)

        return Response(serializer.data)


class ObtainEquipmentCalibrationByEquipmentId(APIView):

    def get(self, request, equipment_id):
        equipment_calibration = models.EquipmentCalibration.objects.filter(Equipment=equipment_id)
        serializer = serializers.EquipmentCalibrationSerializer(equipment_calibration, many=True)

        return Response(serializer.data)


class ObtainEquipmentCalibrationByNextMonth(APIView):
    def get(self, request):
        equipment_calibration = models.EquipmentCalibration.objects.filter(
            scheduled_date__range=[date.today(), date.today() + timedelta(days=30)]
        )
        serializer = serializers.EquipmentCalibrationSerializer(equipment_calibration, many=True)
        return Response(serializer.data)


class AddNewEquipmentCalibration(APIView):

    def post(self, request):
        # Creates a new equipment calibration object.
        equipment_calibration = models.EquipmentCalibration.objects.create(
            Equipment=models.Equipment.objects.get(id=request.data['equipment_id']),
            last_calibration_date=request.data['last_calibration_date'],
            scheduled_date=request.data['scheduled_date'],
            done_date=request.data['done_date'],
            next_calibration_date=request.data['next_calibration_date'],
            calibration_details=request.data['calibration_details'],
            agent_name = request.data['agent_name'],
            status=request.data['status']
        )

        # Adds the equipment calibration to the database.
        equipment_calibration.save()

        return Response({'message': 'Equipment calibration created successfully'})


class UpdateEquipmentCalibration(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.
    def post(self, request):
        # Get the equipment calibration to update.
        equipment_calibration = models.EquipmentCalibration.objects.get(id=request.data['id'])

        # Update the equipment calibration's fields.
        equipment_calibration.last_calibration_date = request.data['last_calibration_date']
        equipment_calibration.scheduled_date = request.data['scheduled_date']
        equipment_calibration.done_date = request.data['done_date']
        equipment_calibration.next_calibration_date = request.data['next_calibration_date']
        equipment_calibration.calibration_details = request.data['calibration_details']
        equipment_calibration.agent_name = request.data['agent_name']
        equipment_calibration.status = request.data['status']

        # Save the equipment calibration to the database.
        equipment_calibration.save()

        return Response({'message': 'Equipment calibration updated successfully'})

