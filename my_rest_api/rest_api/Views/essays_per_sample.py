from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models
import sympy as sp
from datetime import date
import json


def calculated_formulas_based_on_input_fields(input_fields, calculations_forms):
    # This function will be used to calculate the formulas of the essays based on the input fields.
    # The input fields will be used to create a dictionary with the input fields as keys and the values as the
    # input values given by the user.
    # The calculations_forms will be used to create a dictionary with the formulas as keys and the values as the
    # formulas given by the user.
    # The formulas will be calculated using the input fields and the values given by the user.
    # The results will be returned as a dictionary with the formulas as keys and the results as values.

    # First, we get the symbols from the input fields and the given formulas.

    # Defines a dictionary to store the symbols
    symbols_env = {}

    # Defines a dictionary to store the values of all the symbols.

    values_dict = {}

    # Defines the symbols to use in formulas and the values in a values dictionary

    # First, the input fields, which we know they have a value.
    for field in input_fields:
        symbols_env[field['field_symbol']] = sp.symbols(field['field_symbol'])
        values_dict[field['field_symbol']] = field['field_value']

    # Then, the formulas, which we know they don't have a value yet.
    for formula in calculations_forms:
        symbols_env[formula['formula_symbol']] = sp.symbols(formula['formula_symbol'])
        values_dict[formula['formula_symbol']] = "none"

    # Defines a dictionary to store the results of the calculations
    results = []

    # We define a variable to control the loop that
    not_calculated = True

    while not_calculated:

        not_calculated = False

        for formula in calculations_forms:

            if values_dict[formula['formula_symbol']] != "none":
                continue
            else:
                # We select the formula to calculate
                expr = sp.sympify(formula['formula'], locals=symbols_env)
                print(expr)

                # Now, we evaluate the formula with the current values that we have.
                evaluated_expr = expr.subs(values_dict)
                print(evaluated_expr)

                # We check if the formula returns a value or if it returns a formula that needs to be calculated.
                if evaluated_expr.is_number:
                    result_body = {
                        "formula_name": formula['formula_name'],
                        "formula_symbol": formula['formula_symbol'],
                        "result": int(evaluated_expr)
                    }
                    results.append(result_body)
                    values_dict[formula['formula_symbol']] = evaluated_expr
                else:
                    not_calculated = True

    return results


class ObtainAllEssaysPerSample(APIView):
    # This view will be used to obtain all the essays per sample that are stored in the database.
    # The essays per sample will be returned as a list of dictionaries, where each dictionary will contain the
    # information of each essay per sample.
    def get(self, request):
        # First, we get all the essays per sample that are stored in the database.
        essays_per_sample = models.EssayPerSample.objects.all()

        # Then, we serialize the essays per sample.
        serializer = serializers.EssayPerSampleSerializer(essays_per_sample, many=True)

        # Finally, we return the serialized essays per sample.
        return Response(serializer.data)


class ObtainEssayPerSampleByQuoteID(APIView):

    def get(self, request, quote_id):
        # First, we get all the essays per sample that are related to the quote.

        samples_per_quote = models.SamplePerQuote.objects.filter(Quote=quote_id)

        # Then, we get all the essays per sample that are related to the samples per quote.
        essays_per_sample = models.EssayPerSample.objects.filter(Sample__in=samples_per_quote)

        # Then, we serialize the essays per sample.
        serializer = serializers.EssayPerSampleSerializer(essays_per_sample, many=True)

        # Finally, we return the serialized essays per sample.
        return Response(serializer.data)


class ObtainEssayPerSampleByStatus(APIView):

        def get(self, request, status):

            essays_per_sample = models.EssayPerSample.objects.filter(status=status)

            # Then, we serialize the essays per sample.
            serializer = serializers.EssayPerSampleSerializer(essays_per_sample, many=True)

            # Finally, we return the serialized essays per sample.
            return Response(serializer.data)


class ObtainEssayPerSampleByUser(APIView):

        def get(self, request, user_id):
            # This view will be used to obtain all the essays per sample that are assigned to a specific user.
            # The essays per sample will be returned as a list of dictionaries, where each dictionary will contain the
            # information of each essay per sample.
            # First, we get all the essays per sample that are assigned to the user.

            essays_per_sample = models.EssayPerSample.objects.filter(assigned_to=user_id)

            # Then, we serialize the essays per sample.
            serializer = serializers.EssayPerSampleSerializer(essays_per_sample, many=True)

            # Finally, we return the serialized essays per sample.
            return Response(serializer.data)


class UpdateEssayPerSample(APIView):

    def put(self, request):

        essay_per_sample = models.EssayPerSample.objects.get(id=request.data['id'])
        if request.data['assigned_to'] is not None:
            essay_per_sample.assigned_to = models.LaboratoryStaff.objects.get(id=request.data['assigned_to'])
        else:
            essay_per_sample.assigned_to = None
        essay_per_sample.scheduled_date = request.data['scheduled_date']
        essay_per_sample.done_date = request.data['done_date']
        essay_per_sample.status = request.data['status']

        essay_per_sample.save()

        serializer = serializers.EssayPerSampleSerializer(essay_per_sample)

        return Response(serializer.data, status=200)


class ObtainEssayPerSampleById(APIView):

        def get(self, request, essay_per_sample_id):
            essay_per_sample = models.EssayPerSample.objects.get(id=essay_per_sample_id)
            serializer = serializers.EssayPerSampleSerializer(essay_per_sample)

            return Response(serializer.data, status=200)


class CalculateEssayPerSampleResults(APIView):

        def post(self, request):

            # First, we get the input fields and the formulas to calculate the results of the essays per sample.
            essay_per_sample = models.EssayPerSample.objects.get(id=request.data['essay_per_sample_id'])

            if essay_per_sample.status == "COMPLETED":
                return Response("The essay per sample has already been completed.", status=400)

            if essay_per_sample is None:
                return Response("The essay per sample does not exist.", status=400)

            calculation_forms = essay_per_sample.Essay.calculations_forms
            input_fields = request.data['input_fields']

            # Then, we calculate the results of the essays per sample based on the input fields and the formulas.

            results = calculated_formulas_based_on_input_fields(input_fields, calculation_forms)
            essay_per_sample.done_date = date.today()
            essay_per_sample.results = results
            essay_per_sample.status = "COMPLETED"
            essay_per_sample.save()
            # Finally, we return the results of the essays per sample.

            return Response(results, status=200)

