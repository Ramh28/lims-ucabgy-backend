from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .. import serializers, models


# material use related classes.
class ObtainAllMaterialUse(APIView):

    def get(self, request):
        material_use= models.MaterialUses.objects.all()
        serializer = serializers.MaterialUseSerializer(material_use, many=True)

        return Response(serializer.data)


class UpdateMaterialUse(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.
    def post(self, request):
        # Get the material use to update.
        material_use = models.MaterialUses.objects.get(id=request.data['id'])

        # Get the material to update.
        material = models.Materials.objects.get(id=request.data['material_id'])
        # Update the material's amount.
        material.amount = material.amount - abs(material_use.amount-float(request.data['amount']))

        # Update the material use's fields.
        material_use.Materials = models.Materials.objects.get(id=request.data['material_id'])
        material_use.LaboratoryStaff = models.LaboratoryStaff.objects.get(email=request.data['lab_staff_email'])
        material_use.amount = request.data['amount']
        material_use.use_date = request.data['use_date']
        material_use.use_details = request.data['use_details']

        # Save the material to the database.
        material.save()

        # Save the material use to the database.
        material_use.save()

        return Response({'message': 'material use updated successfully'})


class AddNewMaterialUse(APIView):
    def post(self, request):
        laboratoryStaff = models.LaboratoryStaff.objects.get(email=request.data['lab_staff_email'])
        # Creates a new material use object.
        material_use = models.MaterialUses.objects.create(
            Materials=models.Materials.objects.get(id=request.data['material_id']),
            LaboratoryStaff=laboratoryStaff,
            amount=request.data['amount'],
            use_date=request.data['use_date'],
            use_details=request.data['use_details']
        )

        # Get the material to update.
        material = models.Materials.objects.get(id=request.data['material_id'])
        # Update the material's amount.
        material.amount = material.amount - float(request.data['amount'])
        # Save the material to the database.
        material.save()

        # Adds the material use to the database.
        material_use.save()

        return Response({'message': 'material use created successfully'})

class ObtainMaterialUseById(APIView):
    def get(self, request, material_use_id):
        material_use = models.MaterialUses.objects.get(id=material_use_id)
        serializer = serializers.MaterialUseSerializer(material_use)

        return Response(serializer.data)

