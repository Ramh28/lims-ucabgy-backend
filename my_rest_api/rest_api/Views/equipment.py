from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models


# Equipment related classes.
class ObtainAllEquipment(APIView):

    def get(self, request):
        equipment = models.Equipment.objects.all()
        serializer = serializers.EquipmentSerializer(equipment, many=True)

        return Response(serializer.data)


class ObtainEquipmentById(APIView):
    def get(self, request, equipment_id):
        equipment = models.Equipment.objects.get(id=equipment_id)
        serializer = serializers.EquipmentSerializer(equipment)

        return Response(serializer.data)


class AddNewEquipment(APIView):

    def post(self, request):
        # Creates a new equipment object.
        admin = models.LaboratoryStaff.objects.get(email=request.data['admin_email'])

        equipment = models.Equipment.objects.create(
            name=request.data['name'],
            description=request.data['description'],
            brand=request.data['brand'],
            model=request.data['model'],
            serial_number=request.data['serial_number'],
            introduction_date=request.data['introduction_date'],
            location=request.data['location'],
            features=request.data['features'],
            admin=admin
        )

        # Adds the equipment to the database.
        equipment.save()

        return Response({'message': 'Equipment created successfully'})


class UpdateEquipment(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.

    def post(self, request):
        # Get the equipment to update.
        equipment = models.Equipment.objects.get(id=request.data['id'])

        # Update the equipment's fields.
        equipment.name = request.data['name']
        equipment.description = request.data['description']
        equipment.brand = request.data['brand']
        equipment.model = request.data['model']
        equipment.serial_number = request.data['serial_number']
        equipment.introduction_date = request.data['introduction_date']
        equipment.location = request.data['location']
        equipment.features = request.data['features']

        # Save the equipment to the database.
        equipment.save()

        return Response({'message': 'Equipment updated successfully'})

