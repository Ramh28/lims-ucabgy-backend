from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from .. import serializers, models
from decimal import Decimal


# material order related classes.
class ObtainAllMaterialOrder(APIView):

    def get(self, request):
        material_order = models.MaterialOrders.objects.all()
        serializer = serializers.MaterialOrderSerializer(material_order, many=True)

        return Response(serializer.data)


class UpdateMaterialOrder(APIView):
    # Desde el front, aplicar logica de actualizacion de campos.
    def post(self, request):
        # Get the material order to update.
        material_order = models.MaterialOrders.objects.get(id=request.data['id'])

        # Get the material to update.
        material = models.Materials.objects.get(id=request.data['material_id'])
        # Update the material's amount.
        material.amount = material.amount + abs(material_order.amount - float(request.data['amount']))

        # Update the material order's fields.
        material_order.Materials = models.Materials.objects.get(id=request.data['material_id'])
        material_order.LaboratoryStaff = models.LaboratoryStaff.objects.get(email=request.data['lab_staff_email'])
        material_order.amount = request.data['amount']
        material_order.order_date = request.data['order_date']

        # Save the material to the database.
        material.save()

        # Save the material order to the database.
        material_order.save()

        return Response({'message': 'material order updated successfully'})


class AddNewMaterialOrder(APIView):
    def post(self, request):
        laboratoryStaff = models.LaboratoryStaff.objects.get(email=request.data['lab_staff_email'])
        # Creates a new material order object.

        material_order = models.MaterialOrders.objects.create(
            Materials=models.Materials.objects.get(id=request.data['material_id']),
            LaboratoryStaff=laboratoryStaff,
            amount=request.data['amount'],
            order_date=request.data['order_date']
        )

        # Get the material to update.
        material = models.Materials.objects.get(id=request.data['material_id'])
        # Update the material's amount.
        material.amount = material.amount + float(request.data['amount'])
        # Save the material to the database.
        material.save()

        # Adds the material order to the database.
        material_order.save()

        return Response({'message': 'material order created successfully'})


class ObtainMaterialOrderById(APIView):
    def get(self, request, material_order_id):
        material_order = models.MaterialOrders.objects.get(id=material_order_id)
        serializer = serializers.MaterialOrderSerializer(material_order)

        return Response(serializer.data)
