from rest_framework.views import APIView
from rest_framework.response import Response
from .. import serializers, models


def get_user_phone_number(user):
    if not models.PhoneNumber.objects.filter(LaboratoryStaff=user):
        return None
    else:
        return models.PhoneNumber.objects.get(LaboratoryStaff=user).number


# User related classes.
class ObtainAllUsersPerQuantity(APIView):
    def get(self, request, quantity):
        users = models.LaboratoryStaff.objects.all()[:quantity]

        for user in users:
            user.phone_number = get_user_phone_number(user)

        serializer = serializers.LaboratoryStaffSerializer(users, many=True)

        return Response(serializer.data)


class ObtainAllUsers(APIView):

    def get(self, request):
        users = models.LaboratoryStaff.objects.all()

        for user in users:
            user.phone_number = get_user_phone_number(user)

        serializer = serializers.LaboratoryStaffSerializer(users, many=True)

        return Response(serializer.data)


class AddNewUser(APIView):
    def post(self, request):
        # Creates a user object.
        user = models.LaboratoryStaff.objects.create(
            username=request.data['username'],
            email=request.data['email'],
            first_name=request.data['first_name'],
            last_name=request.data['last_name'],
            cdi=request.data['cdi'],
            role=request.data['role'],
            status=request.data['status'],
            password=request.data['password']
        )

        # Creates a phone number object for the user.
        models.PhoneNumber.objects.create(
            number=request.data['phone_number'],
            LaboratoryStaff=user
        )
        # Adds the user to the database.
        user.set_password(request.data['password'])
        user.save()

        # Adds user to the group based on their role.
        if user.role == 'ADMN':
            user.groups.add(1)
        elif user.role == 'LABH':
            user.groups.add(2)
        elif user.role == 'LABS':
            user.groups.add(3)

        return Response({'message': 'User created successfully'})


class UpdateUser(APIView):
    def post(self, request):
        # Del lado del front hacer las validaciones correspondientes para que se envién los datos actualizados y se
        # mantengan los que no. Get the user to update.
        user = models.LaboratoryStaff.objects.get(id=request.data['id'])

        # Update the user's fields.
        user.username = request.data['username']
        user.email = request.data['email']
        user.first_name = request.data['first_name']
        user.last_name = request.data['last_name']
        user.cdi = request.data['cdi']
        user.role = request.data['role']
        user.status = request.data['status']

        # Update the user's password.
        user.set_password(request.data['password'])

        # Save the user to the database.
        user.save()

        # Update the user's phone number.
        if models.PhoneNumber.objects.filter(LaboratoryStaff=user):
            phone_number = models.PhoneNumber.objects.get(LaboratoryStaff=user)
            phone_number.number = request.data['phone_number']
            phone_number.save()
        else:
            models.PhoneNumber.objects.create(
                number=request.data['phone_number'],
                LaboratoryStaff=user
            )

        return Response({'message': 'User updated successfully'})
