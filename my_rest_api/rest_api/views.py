from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from . import models


def get_user_phone_number(user):
    if not models.PhoneNumber.objects.filter(LaboratoryStaff=user):
        return None
    else:
        return models.PhoneNumber.objects.get(LaboratoryStaff=user).number


# Custom token view.
class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        data['user_role'] = self.user.role
        data['user_email'] = self.user.email
        data['user_firstname'] = self.user.first_name
        data['user_lastname'] = self.user.last_name
        data['user_cdi'] = self.user.cdi
        data['user_status'] = self.user.status
        data['user_phone_number'] = get_user_phone_number(self.user)

        return data


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer
