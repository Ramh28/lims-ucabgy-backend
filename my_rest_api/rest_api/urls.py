# rest_api URLS

# Path: my_rest_api/rest_api/urls.py

from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView
from . import views
from .Views import users, quotes, equipment_calibration, equipment_maintenance, equipment, client, essays, sample
from .Views import essays_per_sample, material, material_order, material_use


urlpatterns = [
    # path for token
    path('token', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh', TokenRefreshView.as_view(), name='token_refresh'),

    # path for users
    path('users', users.ObtainAllUsers.as_view(), name='users'),
    path('users/<int:quantity>', users.ObtainAllUsersPerQuantity.as_view(), name='users'),
    path('user/add-new', users.AddNewUser.as_view(), name='add_user'),
    path('user/uptdate', users.UpdateUser.as_view(), name='update_user'),

    # path for equipment
    path('equipment', equipment.ObtainAllEquipment.as_view(), name='equipment'),
    path('equipment/<int:equipment_id>', equipment.ObtainEquipmentById.as_view(), name='equipment_by_id'),
    path('equipment/add', equipment.AddNewEquipment.as_view(), name='add_equipment'),
    path('equipment/update', equipment.UpdateEquipment.as_view(), name='update_equipment'),

    # path for Essays.
    path('essays', essays.ObtainAllEssays.as_view(), name='essays'),
    path('essay/<int:essay_id>', essays.ObtainEssayById.as_view(), name='essay_by_id'),

    # path for Essays per Quote and Sample.
    # return all info related to Essays per Sample
    path('essays-per-sample', essays_per_sample.ObtainAllEssaysPerSample.as_view(), name='essays_per_quote'),

    # Path for essay per sample by ID
    # return all info related to an Essay per Sample by its ID
    path('essay-per-sample/<int:essay_per_sample_id>', essays_per_sample.ObtainEssayPerSampleById.as_view(),
         name='essay_per_sample_by_id'
         ),

    # Path to calculate the results of an essay per sample
    path('essays-per-sample/resolve-essay',
         essays_per_sample.CalculateEssayPerSampleResults.as_view(),
         name='add_essays_per_sample'),

    # return all info related to Essays per Sample by Quote ID
    # Uses this to return all essays per sample related to a specific quote.
    path('essays-per-sample/quote/<int:quote_id>',
         essays_per_sample.ObtainEssayPerSampleByQuoteID.as_view(),
         name='essays_per_quote_by_quote_id'
         ),

    # return all info related to Essays per Sample by Status
    # Uses this to return all essays per sample that have a specific status.
    path('essays-per-sample/status/<str:status>',
         essays_per_sample.ObtainEssayPerSampleByStatus.as_view(),
         name='essays_per_quote_by_status'
         ),

    # return all info related to Essays per Sample by User
    path('essays-per-sample/assigned-to/<int:user_id>',
         essays_per_sample.ObtainEssayPerSampleByUser.as_view(),
         name='essays_per_quote_by_user'
         ),

    # Uses this to update the info related to an essay per sample.
    path('essay-per-sample/update', essays_per_sample.UpdateEssayPerSample.as_view(), name='update_essays_per_sample'),

    # path for equipment maintenance
    path('equipment-maintenance',
         equipment_maintenance.ObtainAllEquipmentMaintenance.as_view(),
         name='equipment_maintenance'),
    path('equipment-maintenance/add',
         equipment_maintenance.AddNewEquipmentMaintenance.as_view(),
         name='add_equipment_maintenance'),
    path('equipment-maintenance/update',
         equipment_maintenance.UpdateEquipmentMaintenance.as_view(),
         name='update_equipment_maintenance'
         ),
    path('equipment-maintenance/equipment/<int:equipment_id>',
         equipment_maintenance.ObtainEquipmentMaintenanceByEquipmentId.as_view(),
         name='equipment_maintenance_by_equipment_id'
         ),
    path('equipment-maintenance/<int:equipment_maintenance_id>',
         equipment_maintenance.ObtainEquipmentMaintenanceById.as_view(),
         name='equipment_maintenance_by_id'
         ),
    path('equipment-maintenance-by-date',
         equipment_maintenance.ObtainEquipmentMaintenanceByNextMonth.as_view(),
         name='equipment_maintenance_by_date'
         ),

    # path for equipment calibration
    path('equipment-calibration',
         equipment_calibration.ObtainAllEquipmentCalibration.as_view(),
         name='equipment_calibration'),

    path('equipment-calibration/add',
         equipment_calibration.AddNewEquipmentCalibration.as_view(),
         name='add_equipment_calibration'),

    path('equipment-calibration/update',
         equipment_calibration.UpdateEquipmentCalibration.as_view(),
         name='update_equipment_calibration'),

    path('equipment-calibration/equipment/<int:equipment_id>',
         equipment_calibration.ObtainEquipmentCalibrationByEquipmentId.as_view(),
         name='equipment_calibration_by_equipment_id'
         ),

    path('equipment-calibration/<int:equipment_calibration_id>',
         equipment_calibration.ObtainEquipmentCalibrationById.as_view(),
         name='equipment_calibration_by_id'
         ),

    path('equipment-calibration-by-date',
         equipment_calibration.ObtainEquipmentCalibrationByNextMonth.as_view(),
         name='equipment_calibration_by_date'
         ),

    # path for clients
    path('clients', client.ObtainAllClients.as_view(), name='clients'),
    path('clients/add', client.AddNewClient.as_view(), name='add_client'),
    path('client/request', client.ReceivedClientRequest.as_view(), name='client_request'),
    path('clients/update', client.UpdateClient.as_view(), name='update_client'),
    path('client/<int:client_id>', client.ObtainClientById.as_view(), name='client_by_id'),

    # path for quotes
    path('quotes', quotes.ObtainAllQuotes.as_view(), name='quotes'),
    path('quote/<int:quote_id>', quotes.ObtainQuoteByQuoteId.as_view(), name='quote_by_id'),
    path('quotes/add', quotes.AddNewQuote.as_view(), name='add_quote'),
    path('quotes/update', quotes.UpdateQuote.as_view(), name='update_quote'),
    path('quotes/delete/<int:quote_id>', quotes.DeleteQuote.as_view(), name='delete_quote_by_id'),
    path('quotes/client/<int:client_id>', quotes.ObtainQuoteByClientId.as_view(), name='quote_by_client_id'),
    path('quotes/status/<str:status>', quotes.ObtainQuotesByStatus.as_view(), name='quote_by_status'),

    # path for samples types
    path('sample-types', sample.ObtainSampleTypes.as_view(), name='sample-types'),
    path('sample-types/add', sample.AddSampleType.as_view(), name='add-sample-type'),
    path('sample-types/update', sample.UpdateSampleType.as_view(), name='update-sample-type'),

    # path for samples

    path('samples', sample.ObtainSamples.as_view(), name='samples'),
    path('samples/add', sample.AddSample.as_view(), name='add-sample'),
    path('samples/update/', sample.UpdateSample.as_view(), name='update-sample'),

    # path for sample per quotes.

    path('samples/quote/<int:quote_id>', sample.ObtainSamplePerQuoteByQuoteID.as_view(), name='samples_by_quote_id'),
    path('samples/quote/status/<str:status>', sample.ObtainSamplePerQuoteByStatus.as_view(), name='samples_by_status'),
    path('samples/quote/add', sample.AddSamplePerQuote.as_view(), name='add-sample-per-quote'),
    path('samples-per-quote/all', sample.ObtainAllSamplesPerQuote.as_view(), name='all-samples-per-quote'),
    path('samples-per-quote/<int:sample_per_quote_id>', sample.ObtainSamplePerQuoteByID.as_view(), name='sample_per_quote_by_id'),
    path('samples-per-quote/update', sample.UpdateSamplePerQuote.as_view(), name='update-sample-per-quote'),

    # path for materials
    path('materials', material.ObtainAllMaterials.as_view(), name='materials'),
    path('materials/add', material.AddNewMaterial.as_view(), name='add_material'),
    path('materials/update', material.UpdateMaterial.as_view(), name='update_material'),
    path('material/<int:material_id>', material.ObtainMaterialById.as_view(), name='material_by_id'),

    # path for material orders
    path('material-order', material_order.ObtainAllMaterialOrder.as_view(), name='material_order'),
    path('material-order/add', material_order.AddNewMaterialOrder.as_view(), name='add_material_order'),
    path('material-order/update', material_order.UpdateMaterialOrder.as_view(), name='update_material_order'),
    path('material-order/<int:material_id>', material_order.ObtainMaterialOrderById.as_view(), name='material_order_by_id'),

    # path for material uses
    path('material-use', material_use.ObtainAllMaterialUse.as_view(), name='material_use'),
    path('material-use/add', material_use.AddNewMaterialUse.as_view(), name='add_material_use'),
    path('material-use/update', material_use.UpdateMaterialUse.as_view(), name='update_material_use'),
    path('material-use/<int:material_id>', material_use.ObtainMaterialUseById.as_view(), name='material_use_by_id'),
]
