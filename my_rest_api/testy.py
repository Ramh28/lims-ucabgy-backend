import sympy as sp

# Getting the input fields values required for calculations
input_fields = [
    {
        "field_symbol": "d",
        "field_value": 15.6,
    },
    {
        "field_symbol": "V_a",
        "field_value": 50,
    },
    {
        "field_symbol": "M",
        "field_value": 100,
    },
    {
        "field_symbol": "M_o",
        "field_value": 50,
    },
    {
        "field_symbol": "V",
        "field_value": 100,
    },
    {
        "field_symbol": "M_1",
        "field_value": 100,
    },
]

calculations_forms = [
    {
        "formula_name": "Masa del picnómetro lleno con agua a 23 °C ± 2 °C en g",
        "formula": "(d) * (V) + (M_p)",
        "formula_symbol": "M_a"
    },
    {
        "formula_name": "Masa total del picnómetro con la muestra y el agua en g",
        "formula": "(d) * (V_a) + (M) - (M_o)",
        "formula_symbol": "M_p"
    },
]

# Defines a dictionary to store the symbols
symbols_env = {}

# Defines a dictionary to store the values of all the symbols.

values_dict = {}

# Defines the symbols to use in formulas and the values in a values dictionary

# First, the input fields, which we know they have a value.
for field in input_fields:
    symbols_env[field['field_symbol']] = sp.symbols(field['field_symbol'])
    values_dict[field['field_symbol']] = field['field_value']

# Then, the formulas, which we know they don't have a value yet.
for formula in calculations_forms:
    symbols_env[formula['formula_symbol']] = sp.symbols(formula['formula_symbol'])
    values_dict[formula['formula_symbol']] = "none"

# Defines a dictionary to store the results of the calculations
results = []


# We define a variable to control the loop that
not_calculated = True

while not_calculated:

    not_calculated = False

    for formula in calculations_forms:

        if values_dict[formula['formula_symbol']] != "none":
            continue
        else:
            # We select the formula to calculate
            expr = sp.sympify(formula['formula'], locals=symbols_env)
            print(expr)

            # Now, we evaluate the formula with the current values that we have.
            evaluated_expr = expr.subs(values_dict)
            print(evaluated_expr)

            # We check if the formula returns a value or if it returns a formula that needs to be calculated.
            if evaluated_expr.is_number:
                result_body = {
                    "formula_name": formula['formula_name'],
                    "formula_symbol": formula['formula_symbol'],
                    "result": evaluated_expr
                }
                results.append(result_body)
                values_dict[formula['formula_symbol']] = evaluated_expr
            else:
                not_calculated = True

print(results)


